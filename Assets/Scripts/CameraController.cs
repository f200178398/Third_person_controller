﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    ///攝影機對著角色的點
    public Transform lookPoint;
    ///攝影機與角色的距離
    public float distance=3f;
    ///攝影機pitch方位運動的角度範圍
    public Vector2 pitchRange=new Vector2(-60.0f,60.0f);
    ///用單獨的空物體分別控制攝像機水平與垂直方向旋轉(點頭pitch 搖頭Yaw)
    ///鼠標轉換視角的靈敏度
    public float sensitivity=120.0f;

    private Transform pitchObj;
    private Transform yawObj;

    private float pitchValue;
    private float yawValue;
    
    // Start is called before the first frame update
    void Start()
    {
        yawObj=new GameObject("Camera Yaw").transform;
        yawObj.SetParent(lookPoint);
        yawObj.localPosition=Vector3.zero;
        yawObj.localRotation=Quaternion.identity;

        pitchObj=new GameObject("Camera Pitch").transform;
        pitchObj.SetParent(yawObj);
        pitchObj.localPosition=Vector3.zero;
        pitchObj.localRotation=Quaternion.identity;
        transform.SetParent(pitchObj);
        transform.localPosition=new Vector3(0,0,-distance);
        transform.localRotation=Quaternion.identity;

    }

    // Update is called once per frame
    void Update()
    {
       ///滑鼠左右滑，左右看
        yawValue+= Input.GetAxisRaw("Mouse X")*Time.deltaTime*sensitivity;
        yawObj.localRotation=Quaternion.Euler(new Vector3(0,yawValue,0));

        /// pitch滑鼠上下移動 視角俯視 仰視看
        pitchValue+=Input.GetAxisRaw("Mouse Y")*Time.deltaTime*sensitivity;
        pitchValue=Mathf.Clamp(pitchValue,pitchRange.x,pitchRange.y);
        pitchObj.localRotation=Quaternion.Euler(new Vector3(pitchValue,0,0));
        
        //打從攝影機打射線判定 設定攝影機的位置
        Vector3 dir=transform.position-lookPoint.position;
        float dis=dir.magnitude;
        RaycastHit hit;
        if(Physics.Raycast(lookPoint.position,dir,out hit,dis+0.1f)){
             if(hit.transform.tag!="Player"){
                 transform.position=hit.point;
             }
             else{
                 transform.localPosition=Vector3.Lerp(transform.localPosition,new Vector3(0,0,-distance),0.3f);
             }

        }
        else{
                transform.localPosition=Vector3.Lerp(transform.localPosition,new Vector3(0,0,-distance),0.3f);
        }
    }
}
